## Caching

The cache was invented by Maurice walkies in 1965 Storing frequently asking things somewhere to increase the access speed in simple language we call it as if we are researching on something and we need to learn about the things and if we need a book we usually go to the library. every time going to the library and taking books will take a lot of time instead of going every day we can bring the book to our home and read from it to increase our speed and we put that book in our shelf . that shelf we call as cache.it'll help to increase our speed. but like our shelf is small same cache library also is small if we go to youtube for the first time browser will download all things like logo icon thumbnail and all. in that case browser stores data on the local computer on a hard drive. That's why sometimes when we clear cache will increase the speed of the computer. not only browsers processor GPU hard drive and SSD's have cache memory it has some hierarchy at the top it has
1. Cpu register 
2. L1 cache
3. L2 cache
4. L3 cache
5. Main memory
6. SSD
7. HDD
 
Like in our home frequently read a book on our desk rarely used book in stack and old book in storage shelf getting book frequently used is fast because it is in our desk and finding an old book is hard we need to look inside shelf
The cache is everywhere on the software side in the operating system, DNS, Databaseweb browser, and web server. storing data in quick memory helps in to speed up the work
in the real-world example, our shelf has more books on it some we use daily some we don't that time we remove some books this process is called Cache eviction strategy we can remove cache which didn't been used for some time that time we remove that cache another strategy is random replacement it'll delete the random cache memory
Types of cache memory
1. Database cache
2. Entry cache
3. Import cache
4. File system cache

#### The locality of cache memory is
Temporary locality: This is used when some resources are accessed repeatedly
Spatial locality: This refers to accessing various data or sources
Performance of cache memory
Cache memory is important because it improves the efficiency of data retrieval. It stores program instruction and data that are used repeatedly in the operation of programs or information that the CPU is likely to need next. The computer processor can access this information more quickly from the cache than from the main memory. Fast access to these instructions increases the overall speed of the program.
Improved performance and the ability to monitor performance are not just about improving general convenience for the user. As technology advances and is increasingly relied upon in mission-critical scenarios, having speed and reliability becomes crucial. Even a few milliseconds of latency could potentially lead to enormous expenses, depending on the situation. These are the information on cache memory

### Advantages of cache
1. They make everything run faster. 
The key benefit of a cache is that it improves the performance of the system. By storing local copies of web site files, for example, your browser only needs to download that information the first time you visit, and can load the local files on subsequent visits.
2. They save data. 
To help improve performance, apps store recently and frequently used data to the cache. Not only does this allow everything to run faster as previously mentioned, but in some cases it can allow apps to work "offline." For example, if you don't have internet access, an app can rely on cached data to continue to work even without a connection.

3. They store data for later use. 
There's a lot of efficiency in only downloading files once. If a copy of a file is stored in the cache, then the app doesn't need to waste time, battery power, and other resources downloading it a second time. Instead, the app only needs to download changed or new files.

### Disadvantages :
1. They can take up a lot of storage space. 
In principle, a cache is a small repository of files used by an app. But some caches can grow large and limit the free space on your device. Clearing the cache can erase the files and recover a large amount of memory.
2. Bad cache can cause the app to works slowly. 
If there's something wrong with a file stored in the cache, it can cause the app to display data incorrectly, glitch, or even crash. That's why a common solution for browser issues is clearing the cache.

3. Caches can prevent apps from loading the latest version of a web page or other data. Apps are supposed to only use the cache to display files unchanged since the last visit. That doesn't always work, though, and sometimes the only way to see the latest version of a web site or other info is to clear the cache, so the app is forced to download everything from begining



### References  
[Technopedia](https://www.techopedia.com/definition/6307/cache-memory)
[Businessinsider](https://www.businessinsider.in/tech/how-to/what-is-a-cache-a-complete-guide-to-caches-and-their-important-uses-on-your-computer-phone-and-other-devices/articleshow/76837123.cms)
